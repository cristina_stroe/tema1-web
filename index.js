
const FIRST_NAME = "cristina";
const LAST_NAME = "stroe";
const GRUPA = "1083";

/**
 * Make the implementation here
 */
function numberParser(value) {
    var a=Math.floor(value);
    if(typeof(value)=="number"&&(value>Number.MAX_SAFE_INTEGER||value<Number.MIN_SAFE_INTEGER))
    return NaN;
    else
    return parseInt(value);
    if(value===Number.POSITIVE_INFINITY|| value===Number.NEGATIVE_INFINITY)
            return NaN;
    return Math.floor(value);
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    numberParser
}

